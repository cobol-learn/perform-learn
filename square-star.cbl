       IDENTIFICATION DIVISION. 
       PROGRAM-ID. SQURE-STAR.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 SCR-LINE PIC X(80) VALUE SPACES .


       PROCEDURE DIVISION .
       000-BEGIN.
           PERFORM 001-PRINT-START-LINE THRU 001-EXIT 10 TIMES
           GOBACK 
       .

       001-PRINT-START-LINE.
           
           MOVE ALL "*" TO SCR-LINE(1:10).
           DISPLAY SCR-LINE 
       .

       001-EXIT.
           EXIT 
       .